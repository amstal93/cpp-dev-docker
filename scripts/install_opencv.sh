#!/usr/bin/env bash

# @file    install_opencv.sh
# @author  Hayat Rajani  [hayatrajani@gmail.com]

# set cmake version
OPENCV_VERSION=4.3.0

# install dependencies
apt-get update -y && apt-get upgrade -y
apt-get install -y --no-install-recommends \
  # required
    libgtk2.0-dev \
    pkg-config \
    libavcodec-dev \
    libavformat-dev \
    libswscale-dev \
  # optional
    python-dev \
    python-numpy \
    libtbb2 \
    libtbb-dev \
    libjpeg-dev \
    libpng-dev \
    libtiff-dev \
    libjasper-dev \
    libdc1394-22-dev

# download opencv with extra modules
cd /tmp
git clone https://github.com/opencv/opencv.git
cd opencv
git checkout $OPENCV_VERSION
cd ..
git clone https://github.com/opencv/opencv_contrib.git
cd opencv_contrib
git checkout $OPENCV_VERSION

# build and install
cd ../opencv
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local \
    -DOPENCV_EXTRA_MODULES_PATH=/tmp/opencv_contrib/modules ..
make install

# cleanup
cd
rm -rf /tmp/opencv /tmp/opencv_contrib
rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*
